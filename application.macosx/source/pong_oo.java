import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import g4p_controls.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class pong_oo extends PApplet {



Ball ball;
XPaddle paddle1;
XPaddle paddle2;
Score score;
GSlider sdr;
GLabel sdrL;
GSlider sdl;
GLabel sdlL;
GSlider sdrh;
GLabel sdrhL;
GSlider sdlh;
GLabel sdlhL;
float paddleDelta = 20;
float paddleWidth = 10;
int playgroundH = 400;


public void setup() {
  
  // Create object and pass in parameters
  ball            = new Ball(width/2, playgroundH/2, 20);
  paddle1         = new XPaddle(100,"left",5);
  paddle2         = new XPaddle(100,"right",5);
  score           = new Score();
  
  G4P.setCursor(CROSS);
  sdl = new GSlider(this, 30, 420, 200, 100, 15);
  sdlL = new GLabel(this, 60, 430, 200, 15, "1 < Level Left Player > 10");
  sdr = new GSlider(this, 350, 420, 200, 100, 15);
  sdrL = new GLabel(this, 380, 430, 200, 15, "1 < Level Right Player > 10");
  sdlh = new GSlider(this, 30, 520, 200, 100, 15);
  sdlhL = new GLabel(this, 30, 530, 220, 15, "1 < Paddle Height Left Player > 200");
  sdrh = new GSlider(this, 350, 520, 200, 100, 15);
  sdrhL = new GLabel(this, 350, 530, 220, 15, "1 < Paddle Height Right Player > 200");
  makeSliderConfigControls();
 
  

}

public void draw() {
  background(200, 200, 200);
  textSize(180);
  fill(210,210,210);
  text("PONG!", 20, 300);
  line(300, 70, 300, 380);
  score.display();
  ball.move();
  ball.display();
  //@todo -> keypress auslagern in Klasse
  paddle1.move();
  paddle2.move();
  paddle1.display();
  paddle2.display();
  
  fill(220,220,0);
  rect(0, playgroundH, width, height - playgroundH);
  
}

public void handleSliderEvents(GValueControl slider, GEvent event) { 

    paddle1.level = sdl.getValueF()*10;
    println(paddle1.level + "  Level  ");
    paddle2.level = sdr.getValueF()*10;
    println(paddle2.level + "  Level  ");
    paddle1.paddleH = sdlh.getValueF()*200;
    println(paddle1.paddleH + "  height  ");
    paddle2.paddleH = sdrh.getValueF()*200;
    println(paddle2.paddleH + "  height  ");
    
    
    
  //  println(sdl.getValueS() + "    " + event);
  //  println(sdrh.getValueS() + "    " + event);
  // println(sdlh.getValueS() + "    " + event);
}

// based on code from http://processing.org/reference/keyCode.html
public void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      if(paddle2.getPaddleY()>0){
        paddle2.move(paddleDelta);
      }
    } else if (keyCode == DOWN) {
     if(paddle2.getPaddleY()+paddle2.getPaddleH()<playgroundH){ 
      paddle2.move(-paddleDelta);
     }
    }
  }
 
 if (keyPressed) {
    if (key == 'w' || key == 'W') {
      if(paddle1.getPaddleY()>0){
            paddle1.move(paddleDelta);
      }
    }
    
    if (key == 's' || key == 'S') {
           if(paddle1.getPaddleY()+paddle2.getPaddleH()<playgroundH){ 
                paddle1.move(-paddleDelta);
           }
    }
 }
}
class Ball {

  float x;
  float y;
  int diameter;
  float dX = random(2, 10);
  float dY = random(2, 5);
  PImage ballImg;
  PImage ballImg90;
  PImage ballImg180;
  PImage ballImg240;
  
   Ball(float tempX, float tempY, int tempDiameter) {
    x = tempX;
    y = tempY;
    diameter = tempDiameter;
  }
  
  public void reset(){
    
       delay(100);   
    
       x = width / 2;
       y = random (50, 350);
       
       
       if(PApplet.parseInt(random(0, 2)) == 1){
         dX = -dX;
       }
    
  }
  
  public void move() {    
   
    if (collision()){
      dX = -dX; 
    }
    
    if (ballRight()>width) {
    //dX = -dX; // if dX == 2, it becomes -2; if dX is -2, it becomes 2
    score.right();
    reset();
    }
   
    if (ballLeft() < 0) {
    //  dX = -dX; // if dX == 2, it becomes -2; if dX is -2, it becomes 2
    score.left(); 
    reset();
    }
   
    if (ballBottom() > playgroundH) {
      dY = -dY; // if dY == 2, it becomes -2; if dY is -2, it becomes 2
    }
   
    if (ballTop() < 0) {
      dY = -dY; // if dY == 2, it becomes -2; if dY is -2, it becomes 2
    }
    
     x = x + dX;
     y = y + dY;
    
  }
  
  public void display() {
   ballImg = loadImage("240px-Soccerball.svg.png");
   imageMode(CENTER);
   fill(100,100,100);
   ellipse(x+5, y+15, diameter, diameter-10);
   image(ballImg, x, y, 25, 25);
   fill(0,0,0);
  }

 public boolean collision() {
  boolean returnValue = false; // assume there is no collision
  if ((ballRight() >= paddle1.getPaddleX()) && (ballLeft() <= paddle1.getPaddleX() + paddle1.getPaddleW())) {
    if ((ballBottom() >= paddle1.getPaddleY()) && (ballTop() <= paddle1.getPaddleY() + paddle1.getPaddleH())) {
      returnValue = true;
    }
  }
  if ((ballRight() >= paddle2.getPaddleX()) && (ballLeft() <= paddle2.getPaddleX() + paddle2.getPaddleW())) {
    if ((ballBottom() >= paddle2.getPaddleY()) && (ballTop() <= paddle2.getPaddleY() + paddle2.getPaddleH())) {
      returnValue = true;
    }
  }
  return returnValue;
}
  
    public float ballLeft() {
    return x - diameter/2;
  }
   
  public float ballRight() {
    return x + diameter/2;
  }
   
  public float ballTop() {
    return y - diameter/2;
  }
   
  public float ballBottom() {
    return y + diameter/2;
  }
  public float getDX() {
    return dX;
  }
    public float getDY() {
    return dY;
  }

}
class Paddle {

float paddleX;
float paddleH;
float paddleY = playgroundH/2 - paddleH/2;
float paddleW = paddleWidth;
float paddleMover;

   Paddle(float tempheight,String tempPlayerSide) {
    if(tempPlayerSide == "left"){
      paddleX = 0;
    }else{
      paddleX = width - paddleWidth;
    }
     
    paddleH = tempheight;
    
  }
  
  public float getPaddleX(){
   return paddleX;
  }
  
  public float getPaddleY(){
   return paddleY;
  }
  public float getPaddleH(){
   return paddleH;
  }
  public float getPaddleW(){
   return paddleW;
  }
  
  public void move(float paddleMover) {
      paddleY = paddleY - paddleMover;
  
  }
  
  public void display() {
    rect(paddleX, paddleY, paddleW, paddleH);
  }
  


}
class Score{

  int left = 0;
  int right = 0;
  
  Score(){
   
  }
  
  public void left(){
    left = left + 1;
  }
  public void right() {
    right = right + 1;
  }
  public void display(){
    fill(200,0,0);
    rect(220, 5, 160, 50);
    textSize(32);
    fill(255,255,255);
    text(right, 250, 40);
    text(":", 295, 40);
    text(left, 330, 40);
  }
  
} 
class XPaddle extends Paddle {  

  float middle = playgroundH/2 - this.paddleH/2;
  float level = 1;
  String playerSide = "left";
  
  XPaddle(float tempheight,String tempPlayerSide, float templevel) {
    super(tempheight,tempPlayerSide);
     if(tempPlayerSide == "left"){
      paddleX = 0;
    }else{
      paddleX = width - paddleWidth;
    }
    paddleH = tempheight;
    level = templevel;
    playerSide = tempPlayerSide;
  }

  public void move(){
    
    if(playerSide == "left"){
    
        if(ball.getDX()> 0){
      //wenn der Ball sich wegbewegt, geh in die Mitte
        movePaddle2theMiddle();
        }else{
         // wenn der Ball sich auf das Paddle bewegt, den Ball tracken
           move2Ball(level);
        }
    
    
    }else{
        
        if(ball.getDX()< 0){
      //wenn der Ball sich wegbewegt, geh in die Mitte
           movePaddle2theMiddle();
        }else{
         // wenn der Ball sich auf das Paddle bewegt, den Ball tracken
           move2Ball(level);
        }
    
    }
    

        
  }
  
  public void movePaddle2theMiddle() {
      
    if(this.paddleY < middle){
        //Ball ist in der oberen 'Hälfte
              this.paddleY = paddleY + 1;
        }else{
              this.paddleY = paddleY - 1;
        }
  } 
  
  public void move2Ball(float level){
  
       if (ball.ballTop() > this.paddleY){
        
             this.paddleY = this.paddleY + level;
         
        }else{
        
             this.paddleY = this.paddleY - level;
        }
  }

}
public void makeSliderConfigControls() {
//@todo -> slider working
}
  public void settings() {  size(600, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "pong_oo" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
