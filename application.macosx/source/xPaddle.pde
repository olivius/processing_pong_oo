class XPaddle extends Paddle {  

  float middle = playgroundH/2 - this.paddleH/2;
  float level = 1;
  String playerSide = "left";
  
  XPaddle(float tempheight,String tempPlayerSide, float templevel) {
    super(tempheight,tempPlayerSide);
     if(tempPlayerSide == "left"){
      paddleX = 0;
    }else{
      paddleX = width - paddleWidth;
    }
    paddleH = tempheight;
    level = templevel;
    playerSide = tempPlayerSide;
  }

  void move(){
    
    if(playerSide == "left"){
    
        if(ball.getDX()> 0){
      //wenn der Ball sich wegbewegt, geh in die Mitte
        movePaddle2theMiddle();
        }else{
         // wenn der Ball sich auf das Paddle bewegt, den Ball tracken
           move2Ball(level);
        }
    
    
    }else{
        
        if(ball.getDX()< 0){
      //wenn der Ball sich wegbewegt, geh in die Mitte
           movePaddle2theMiddle();
        }else{
         // wenn der Ball sich auf das Paddle bewegt, den Ball tracken
           move2Ball(level);
        }
    
    }
    

        
  }
  
  void movePaddle2theMiddle() {
      
    if(this.paddleY < middle){
        //Ball ist in der oberen 'Hälfte
              this.paddleY = paddleY + 1;
        }else{
              this.paddleY = paddleY - 1;
        }
  } 
  
  void move2Ball(float level){
  
       if (ball.ballTop() > this.paddleY){
        
             this.paddleY = this.paddleY + level;
         
        }else{
        
             this.paddleY = this.paddleY - level;
        }
  }

}
