import g4p_controls.*;

Ball ball;
XPaddle paddle1;
XPaddle paddle2;
Score score;
GSlider sdr;
GLabel sdrL;
GSlider sdl;
GLabel sdlL;
GSlider sdrh;
GLabel sdrhL;
GSlider sdlh;
GLabel sdlhL;
float paddleDelta = 20;
float paddleWidth = 10;
int playgroundH = 400;


void setup() {
  size(600, 600);
  // Create object and pass in parameters
  ball            = new Ball(width/2, playgroundH/2, 20);
  paddle1         = new XPaddle(100,"left",5);
  paddle2         = new XPaddle(100,"right",5);
  score           = new Score();
  
  G4P.setCursor(CROSS);
  sdl = new GSlider(this, 30, 420, 200, 100, 15);
  sdlL = new GLabel(this, 60, 430, 200, 15, "1 < Level Left Player > 10");
  sdr = new GSlider(this, 350, 420, 200, 100, 15);
  sdrL = new GLabel(this, 380, 430, 200, 15, "1 < Level Right Player > 10");
  sdlh = new GSlider(this, 30, 520, 200, 100, 15);
  sdlhL = new GLabel(this, 30, 530, 220, 15, "1 < Paddle Height Left Player > 200");
  sdrh = new GSlider(this, 350, 520, 200, 100, 15);
  sdrhL = new GLabel(this, 350, 530, 220, 15, "1 < Paddle Height Right Player > 200");
  makeSliderConfigControls();
 
  

}

void draw() {
  background(200, 200, 200);
  textSize(180);
  fill(210,210,210);
  text("PONG!", 20, 300);
  line(300, 70, 300, 380);
  score.display();
  ball.move();
  ball.display();
  //@todo -> keypress auslagern in Klasse
  paddle1.move();
  paddle2.move();
  paddle1.display();
  paddle2.display();
  
  fill(220,220,0);
  rect(0, playgroundH, width, height - playgroundH);
  
}

public void handleSliderEvents(GValueControl slider, GEvent event) { 

    paddle1.level = sdl.getValueF()*10;
    println(paddle1.level + "  Level  ");
    paddle2.level = sdr.getValueF()*10;
    println(paddle2.level + "  Level  ");
    paddle1.paddleH = sdlh.getValueF()*200;
    println(paddle1.paddleH + "  height  ");
    paddle2.paddleH = sdrh.getValueF()*200;
    println(paddle2.paddleH + "  height  ");
    
    
    
  //  println(sdl.getValueS() + "    " + event);
  //  println(sdrh.getValueS() + "    " + event);
  // println(sdlh.getValueS() + "    " + event);
}

// based on code from http://processing.org/reference/keyCode.html
void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      if(paddle2.getPaddleY()>0){
        paddle2.move(paddleDelta);
      }
    } else if (keyCode == DOWN) {
     if(paddle2.getPaddleY()+paddle2.getPaddleH()<playgroundH){ 
      paddle2.move(-paddleDelta);
     }
    }
  }
 
 if (keyPressed) {
    if (key == 'w' || key == 'W') {
      if(paddle1.getPaddleY()>0){
            paddle1.move(paddleDelta);
      }
    }
    
    if (key == 's' || key == 'S') {
           if(paddle1.getPaddleY()+paddle2.getPaddleH()<playgroundH){ 
                paddle1.move(-paddleDelta);
           }
    }
 }
}
