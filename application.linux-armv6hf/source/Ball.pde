class Ball {

  float x;
  float y;
  int diameter;
  float dX = random(2, 10);
  float dY = random(2, 5);
  PImage ballImg;
  PImage ballImg90;
  PImage ballImg180;
  PImage ballImg240;
  
   Ball(float tempX, float tempY, int tempDiameter) {
    x = tempX;
    y = tempY;
    diameter = tempDiameter;
  }
  
  void reset(){
    
       delay(100);   
    
       x = width / 2;
       y = random (50, 350);
       
       
       if(int(random(0, 2)) == 1){
         dX = -dX;
       }
    
  }
  
  void move() {    
   
    if (collision()){
      dX = -dX; 
    }
    
    if (ballRight()>width) {
    //dX = -dX; // if dX == 2, it becomes -2; if dX is -2, it becomes 2
    score.right();
    reset();
    }
   
    if (ballLeft() < 0) {
    //  dX = -dX; // if dX == 2, it becomes -2; if dX is -2, it becomes 2
    score.left(); 
    reset();
    }
   
    if (ballBottom() > playgroundH) {
      dY = -dY; // if dY == 2, it becomes -2; if dY is -2, it becomes 2
    }
   
    if (ballTop() < 0) {
      dY = -dY; // if dY == 2, it becomes -2; if dY is -2, it becomes 2
    }
    
     x = x + dX;
     y = y + dY;
    
  }
  
  void display() {
   ballImg = loadImage("240px-Soccerball.svg.png");
   imageMode(CENTER);
   fill(100,100,100);
   ellipse(x+5, y+15, diameter, diameter-10);
   image(ballImg, x, y, 25, 25);
   fill(0,0,0);
  }

 boolean collision() {
  boolean returnValue = false; // assume there is no collision
  if ((ballRight() >= paddle1.getPaddleX()) && (ballLeft() <= paddle1.getPaddleX() + paddle1.getPaddleW())) {
    if ((ballBottom() >= paddle1.getPaddleY()) && (ballTop() <= paddle1.getPaddleY() + paddle1.getPaddleH())) {
      returnValue = true;
    }
  }
  if ((ballRight() >= paddle2.getPaddleX()) && (ballLeft() <= paddle2.getPaddleX() + paddle2.getPaddleW())) {
    if ((ballBottom() >= paddle2.getPaddleY()) && (ballTop() <= paddle2.getPaddleY() + paddle2.getPaddleH())) {
      returnValue = true;
    }
  }
  return returnValue;
}
  
    float ballLeft() {
    return x - diameter/2;
  }
   
  float ballRight() {
    return x + diameter/2;
  }
   
  float ballTop() {
    return y - diameter/2;
  }
   
  float ballBottom() {
    return y + diameter/2;
  }
  float getDX() {
    return dX;
  }
    float getDY() {
    return dY;
  }

}
