class Paddle {

float paddleX;
float paddleH;
float paddleY = playgroundH/2 - paddleH/2;
float paddleW = paddleWidth;
float paddleMover;

   Paddle(float tempheight,String tempPlayerSide) {
    if(tempPlayerSide == "left"){
      paddleX = 0;
    }else{
      paddleX = width - paddleWidth;
    }
     
    paddleH = tempheight;
    
  }
  
  float getPaddleX(){
   return paddleX;
  }
  
  float getPaddleY(){
   return paddleY;
  }
  float getPaddleH(){
   return paddleH;
  }
  float getPaddleW(){
   return paddleW;
  }
  
  void move(float paddleMover) {
      paddleY = paddleY - paddleMover;
  
  }
  
  void display() {
    rect(paddleX, paddleY, paddleW, paddleH);
  }
  


}
